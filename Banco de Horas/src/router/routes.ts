import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', name: 'Home', component: () => import('pages/IndexPage.vue') },
      { path: '/atividade/cadastro/:id?', name: 'AtividadeCadastro', component: () => import('pages/AtividadeCadastro.vue') },
      { path: '/atividade', name: 'Atividade', component: () => import('pages/AtividadeListagem.vue') },
      { path: '/projeto/cadastro/', name: 'projeto', component: () => import('pages/CadastrarProjeto.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
